package edu.byu.colingm.familymap.filter;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import edu.byu.colingm.familymap.R;

/**
 * author: colingm
 * date: 3/25/16.
 */
public class FilterActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_filter);
		getFragmentManager().beginTransaction().replace(android.R.id.content, new FilterFragment()).commit();
	}

	public static class FilterFragment extends PreferenceFragment
	{
		@Override
		public void onCreate(final Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.filters);
		}
	}
}

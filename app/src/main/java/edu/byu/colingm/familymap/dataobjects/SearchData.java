package edu.byu.colingm.familymap.dataobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * author: colingm
 * date: 4/18/16.
 */
public class SearchData
{
	private List<Person> mPersons = new ArrayList<>();
	private List<FactDTO> mFactDTOs = new ArrayList<>();

	public SearchData(List<Person> persons, List<FactDTO> factDTOs)
	{
		mPersons = persons;
		mFactDTOs = factDTOs;
	}

	public SearchData()
	{

	}

	public List<Person> getPersons()
	{
		return mPersons;
	}

	public void setPersons(List<Person> persons)
	{
		mPersons = persons;
	}

	public List<FactDTO> getFactDTOs()
	{
		return mFactDTOs;
	}

	public void setFactDTOs(List<FactDTO> factDTOs)
	{
		mFactDTOs = factDTOs;
	}
}

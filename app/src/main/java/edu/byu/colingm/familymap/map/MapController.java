package edu.byu.colingm.familymap.map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import edu.byu.colingm.familymap.R;
import edu.byu.colingm.familymap.client.UserGet;
import edu.byu.colingm.familymap.dataobjects.FactDTO;
import edu.byu.colingm.familymap.dataobjects.Person;
import edu.byu.colingm.familymap.dataobjects.types.Event;
import edu.byu.colingm.familymap.dataobjects.types.Relationship;
import edu.byu.colingm.familymap.model.FamilyMapModel;
import edu.byu.colingm.familymap.utils.Helper;

import java.util.List;
import java.util.Map;

/**
 * author: colingm
 * date: 3/25/16.
 */
public class MapController implements FactListener
{
	private static final String TAG = "MapController";
	private Activity mView;
	private int mLifeLinesColor;
	private boolean mShowLifeLines;
	private int mFamilyLinesColor;
	private boolean mShowFamilyLines;
	private int mSpouseLinesColor;
	private boolean mShowSpouseLines;

	public MapController(Activity view)
	{
		mView = view;
		FamilyMapModel.model().registerFactListener(this);
	}

	public void initializeMap()
	{
		loadFilters();
		if (!FamilyMapModel.model().isLoaded())
		{
			new UserGet(mView).execute();
		}
		else
		{
			loadAllFacts();
			loadSelectedMarker();
		}
	}

	public void loadSelectedMarker()
	{
		if (FamilyMapModel.model().isMarkerSelected())
		{
			Map<String, String> markerData = FamilyMapModel.model().getCurrentMarker();
			String factId = markerData.get("factId");
			String personId = markerData.get("personId");
			selectMarker(factId, personId);
		}
	}

	public void selectMarker(String factId, String personId)
	{
		Log.e(TAG, "Select Marker - Fact: " + factId + ", Person: " + personId);
		getMap().clearLines();
		FamilyMapModel.model().setCurrentMarker(factId, personId);
		FactDTO fact = FamilyMapModel.model().getFact(factId, personId);
		Person person = FamilyMapModel.model().getPerson(personId);

		getMap().updateEventInformation(person, fact);
		drawEventLines(factId, personId);
	}

	public void drawEventLines(String factId, String personId)
	{
		if (mShowSpouseLines)
		{
			drawSpouseLines(factId, personId);
		}
		if (mShowFamilyLines)
		{
			drawFamilyLines(factId, personId);
		}
		if (mShowLifeLines)
		{
			drawLifeLines(personId);
		}
	}

	public void drawLifeLines(String personId)
	{
		List<FactDTO> facts = FamilyMapModel.model().getLifeFactsForPerson(personId);
		if (facts != null)
		{
			FactDTO fact1 = facts.get(0);
			FactDTO fact2;
			for (int i = 1; i < facts.size(); i++)
			{
				fact2 = facts.get(i);
				drawLine(fact1.getPlace(), fact2.getPlace(), 25, mLifeLinesColor);
				fact1 = fact2;
			}
		}
	}

	public void drawFamilyLines(String factId, String personId)
	{
		FactDTO factDTO = FamilyMapModel.model().getFact(factId, personId);

		Person relative = FamilyMapModel.model().getRelative(personId, Relationship.Father);
		if (relative != null)
			drawFamilyLine(factDTO, relative.getId(), 25);
		relative = FamilyMapModel.model().getRelative(personId, Relationship.Mother);
		if (relative != null)
			drawFamilyLine(factDTO, relative.getId(), 25);
	}

	public void drawFamilyLine(FactDTO fact1, String relativeId, int width)
	{
		FactDTO fact2 = FamilyMapModel.model().getEarliestFactForPerson(relativeId);
		if (width <= 0)
			width = 5;

		if (fact2 != null)
		{
			drawLine(fact1.getPlace(), fact2.getPlace(), width, mFamilyLinesColor);

			Person relative = FamilyMapModel.model().getRelative(relativeId, Relationship.Father);
			if (relative != null)
				drawFamilyLine(fact2, relative.getId(), width - 5);

			relative = FamilyMapModel.model().getRelative(relativeId, Relationship.Mother);
			if (relative != null)
				drawFamilyLine(fact2, relative.getId(), width - 5);
		}
	}

	public void drawSpouseLines(String factId, String personId)
	{
		Person spouse = FamilyMapModel.model().getRelative(personId, Relationship.Spouse);
		if (spouse != null)
		{
			FactDTO spouseFact = FamilyMapModel.model().getEarliestFactForPerson(spouse.getId());
			FactDTO personFact = FamilyMapModel.model().getFact(factId, personId);

			if (spouseFact != null)
			{
				drawLine(personFact.getPlace(), spouseFact.getPlace(), 25, mSpouseLinesColor);
			}
		}
	}

	public void showPersonView(String personId)
	{

	}

	public void drawLine(String address1, String address2, int width, int color)
	{
		LatLng location1 = Helper.getLocationFromAddress(mView, address1);
		LatLng location2 = Helper.getLocationFromAddress(mView, address2);

		getMap().drawLine(location1, location2, width, color);
	}

	public void placeMarker(String address, String title, float color, boolean moveCamera)
	{
		LatLng location = Helper.getLocationFromAddress(mView, address);

		getMap().placeMarker(location, title, color, moveCamera);
	}

	public void loadFilters()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mView);
		FamilyMapModel.model().setFilters(sharedPreferences);
		mLifeLinesColor = sharedPreferences.getInt(mView.getString(R.string.life_story_color_key), 0xFFE70000);
		mFamilyLinesColor = sharedPreferences.getInt(mView.getString(R.string.family_tree_color_key), 0xFF334EE9);
		mSpouseLinesColor = sharedPreferences.getInt(mView.getString(R.string.spouse_lines_color_key), 0xFF008E04);
		mShowLifeLines = sharedPreferences.getBoolean(mView.getString(R.string.life_story_enable_key), true);
		mShowFamilyLines = sharedPreferences.getBoolean(mView.getString(R.string.family_tree_enable_key), true);
		mShowSpouseLines = sharedPreferences.getBoolean(mView.getString(R.string.spouse_lines_enable_key), true);
	}

	public Activity getView()
	{
		return mView;
	}

	public MapActivity getMap()
	{
		return (MapActivity) getView();
	}

	public void loadAllFacts()
	{
		List<FactDTO> facts = FamilyMapModel.model().getVisibleFacts();
		boolean first = true;
		for (FactDTO fact : facts)
		{
			Log.e(TAG, fact.toString());
			Event type = fact.getType();
			float color = 0.0f;
			switch (type)
			{
				case Birth:
					color = BitmapDescriptorFactory.HUE_GREEN;
					break;
				case Christening:
					color = BitmapDescriptorFactory.HUE_RED;
					break;
				case Death:
					color = BitmapDescriptorFactory.HUE_BLUE;
					break;
				case Unknown:
					color = BitmapDescriptorFactory.HUE_ORANGE;
					break;
				default:
					Log.e(TAG, "Incorrect event type");
			}

			placeMarker(fact.getPlace(), fact.getId() + "::" + fact.getPersonId(), color, first);
			first = false;
		}
	}

	@Override
	public void notifyDoneLoading()
	{
		loadAllFacts();
	}
}

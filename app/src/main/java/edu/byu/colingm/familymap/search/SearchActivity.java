package edu.byu.colingm.familymap.search;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import edu.byu.colingm.familymap.R;

/**
 * author: colingm
 * date: 3/25/16.
 */
public class SearchActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_search);
		EditText viewById = (EditText)findViewById(R.id.search_src_text);
		if (viewById != null)
			viewById.setKeyListener(keyListener);
	}

	private KeyListener keyListener = new KeyListener()
	{
		@Override
		public int getInputType()
		{
			return 0;
		}

		@Override
		public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event)
		{
			Log.e("SearchActivity", "Key Down");
			return false;
		}

		@Override
		public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event)
		{
			return false;
		}

		@Override
		public boolean onKeyOther(View view, Editable text, KeyEvent event)
		{
			return false;
		}

		@Override
		public void clearMetaKeyState(View view, Editable content, int states)
		{

		}
	};
}

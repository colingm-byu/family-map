package edu.byu.colingm.familymap.map;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import edu.byu.colingm.familymap.R;
import edu.byu.colingm.familymap.dataobjects.FactDTO;
import edu.byu.colingm.familymap.dataobjects.Person;
import edu.byu.colingm.familymap.dataobjects.types.Gender;
import edu.byu.colingm.familymap.filter.FilterActivity;
import edu.byu.colingm.familymap.search.SearchActivity;
import edu.byu.colingm.familymap.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * author: colingm
 * date: 3/24/16.
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback
{

	private GoogleMap mMap;
	private MapController mController;
	private SharedPreferences mSharedPreferences;
	private int mMapType;
	private List<Polyline> mPolylines = new ArrayList<>();

	public void placeMarker(LatLng location, String title, float color, boolean moveCamera)
	{
		if (mMap == null)
			return;

		MarkerOptions marker = new MarkerOptions().position(location)
				.title(title)
				.icon(BitmapDescriptorFactory.defaultMarker(color));

		mMap.addMarker(marker);

		if (moveCamera)
			mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
	}

	public void drawLine(LatLng address1, LatLng address2, int width, int color)
	{
		if (mMap == null)
			return;

		mPolylines.add(mMap.addPolyline(new PolylineOptions().add(address1, address2)
				.width(width)
				.color(color)
				.geodesic(false)));
	}

	public void clearLines()
	{
		for (Polyline line : mPolylines)
			line.remove();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		mController = new MapController(this);
		mController.initializeMap();
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mMapType = Integer.parseInt(mSharedPreferences.getString(getString(R.string.map_type_key), "1"));
	}

	@Override
	public void onMapReady(GoogleMap googleMap)
	{
		mMap = googleMap;
		mMap.setMapType(mMapType);
		mMap.setOnMarkerClickListener(mMarkerClickListener);
		mMap.getUiSettings().setMapToolbarEnabled(false);
		mController.notifyDoneLoading();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent intent;
		switch (item.getItemId())
		{
			case R.id.action_search:
				intent = new Intent(MapActivity.this, SearchActivity.class);
				startActivity(intent);
				return true;
			case R.id.action_filter:
				intent = new Intent(MapActivity.this, FilterActivity.class);
				startActivity(intent);
				return true;
			case R.id.action_settings:
				intent = new Intent(MapActivity.this, SettingsActivity.class);
				startActivity(intent);
				return true;
			default:
				// If we got here, the user's action was not recognized.
				// Invoke the superclass to handle it.
				return super.onOptionsItemSelected(item);

		}
	}

	public void updateEventInformation(final Person person, FactDTO factDTO)
	{
		TextView eventDescription = (TextView)findViewById(R.id.event_description);
		String description = person.getName() + "\n" +
				factDTO.getType().toString() + ": " +
				factDTO.getPlace() + " (" + factDTO.getDate() + ")";

		if (eventDescription != null)
			eventDescription.setText(description);

		ImageView genderImage = (ImageView)findViewById(R.id.genderImage);
		if (genderImage != null)
		{
			if (person.getGender() == Gender.Male)
				genderImage.setImageDrawable(getDrawable(R.drawable.gender_male));
			else
				genderImage.setImageDrawable(getDrawable(R.drawable.gender_female));
		}

		EventFragment eventFragment = (EventFragment)getFragmentManager().findFragmentById(R.id.event);
		eventFragment.updateEventInformation(person, factDTO);
	}

	private GoogleMap.OnMarkerClickListener mMarkerClickListener = new GoogleMap.OnMarkerClickListener()
	{
		@Override
		public boolean onMarkerClick(Marker marker)
		{
			String title = marker.getTitle();
			String[] splitTitle = title.split("::");

			String factId = splitTitle[0];
			String personId = splitTitle[1];

			mController.selectMarker(factId, personId);
			mMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

			return true;
		}
	};
}


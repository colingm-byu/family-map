package edu.byu.colingm.familymap.dataobjects;

import edu.byu.colingm.familymap.dataobjects.types.Event;

/**
 * author: colingm
 * date: 4/5/16.
 */
public class FactDTO
{
	private String id;
	private String personId;
	private String type;
	private NormalizedDTO date;
	private NormalizedDTO place;

	public FactDTO()
	{
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Event getType()
	{
		if (type.contains("Birth"))
			return Event.Birth;
		else if (type.contains("Christening"))
			return Event.Christening;
		else if (type.contains("Death"))
			return Event.Death;
		else
			return Event.Unknown;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getDate()
	{
		return date.getValue();
	}

	public void setDate(NormalizedDTO date)
	{
		this.date = date;
	}

	public String getPlace()
	{
		return place.getValue();
	}

	public void setPlace(NormalizedDTO place)
	{
		this.place = place;
	}

	public String getPersonId()
	{
		return personId;
	}

	public void setPersonId(String personId)
	{
		this.personId = personId;
	}

	@Override
	public String toString()
	{
		return "FactDTO{" +
				"id='" + id + '\'' +
				", type='" + type + '\'' +
				", date=" + date +
				", place=" + place +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FactDTO factDTO = (FactDTO) o;

		if (id != null ? !id.equals(factDTO.id) : factDTO.id != null) return false;
		if (type != null ? !type.equals(factDTO.type) : factDTO.type != null) return false;
		if (date != null ? !date.equals(factDTO.date) : factDTO.date != null) return false;
		return !(place != null ? !place.equals(factDTO.place) : factDTO.place != null);

	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (place != null ? place.hashCode() : 0);
		return result;
	}
}
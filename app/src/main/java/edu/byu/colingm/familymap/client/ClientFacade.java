package edu.byu.colingm.familymap.client;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.byu.colingm.familymap.dataobjects.Person;
import edu.byu.colingm.familymap.dataobjects.User;
import edu.byu.colingm.familymap.exceptions.BadRequestException;
import edu.byu.colingm.familymap.model.FamilyMapModel;
import edu.byu.colingm.familymap.utils.Helper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * author: colingm
 * date: 3/31/16.
 */
public class ClientFacade
{
	private static final String API_CONFIG = "fs_api_uri";
	private static final Type LIST_USER_TYPE = new TypeToken<List<User>>()
	{
	}.getType();
	private static final Type LIST_PERSON_TYPE = new TypeToken<List<Person>>()
	{
	}.getType();

	private String baseURL;

	private String getToken()
	{
		return FamilyMapModel.model().getAccessToken();
	}

	private String getBaseURL()
	{
		if (baseURL == null)
			baseURL = Helper.getConfigValue(API_CONFIG);

		return baseURL;
	}

	public User getCurrentUser()
	{
		String url = getBaseURL() + "/users/current";

		try
		{
			String result = performGet(getToken(), url);

			JSONObject json = new JSONObject(result);
			List<User> users = new Gson().fromJson(json.getString("users"), LIST_USER_TYPE);
			return users.get(0);
		}
		catch (BadRequestException | JSONException ex)
		{
			return null;
		}
	}

	public Person getPerson(String personId)
	{
		String url = getBaseURL() + "/tree/persons/" + personId;

		try
		{
			String result = performGet(getToken(), url);

			JSONObject json = new JSONObject(result);
			List<Person> persons = new Gson().fromJson(json.getString("persons"), LIST_PERSON_TYPE);
			return persons.get(0);
		}
		catch (BadRequestException | JSONException ex)
		{
			return null;
		}
	}

	public List<Person> getRelatedPersons(String personId, boolean spouses)
	{
		String url = getBaseURL() + "/tree/persons/" + personId;

		if (spouses)
			url += "/spouses";
		else
			url += "/parents";

		try
		{
			String result = performGet(getToken(), url);

			JSONObject json = new JSONObject(result);
			return new Gson().fromJson(json.getString("persons"), LIST_PERSON_TYPE);
		}
		catch (BadRequestException | JSONException ex)
		{
			return null;
		}
	}

	private String performGet(String token, String uri) throws BadRequestException
	{
		HttpURLConnection connection;

		try
		{
			URL url = new URL(uri);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Accept", "application/x-gedcomx-v1+json");
			connection.setRequestProperty("Authorization", "Bearer " + token);
			connection.setRequestMethod("GET");

			InputStreamReader isr = new InputStreamReader(connection.getInputStream());
			BufferedReader reader = new BufferedReader(isr);

			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null)
			{
				sb.append(line).append("\n");
			}
			// Response from server after login process will be stored in response variable.
			String response = sb.toString();

			isr.close();
			reader.close();

			return response;
		}
		catch (IOException e)
		{
			throw new BadRequestException();
		}
	}

	private String performPost(String token, String uri, String body) throws BadRequestException
	{
		HttpURLConnection connection;
		OutputStreamWriter request;

		try
		{
			URL url = new URL(uri);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/x-gedcomx-v1+json");
			connection.setRequestProperty("Authorization", "Bearer " + token);
			connection.setRequestMethod("POST");

			request = new OutputStreamWriter(connection.getOutputStream());
			request.write(body);
			request.flush();
			request.close();
			InputStreamReader isr = new InputStreamReader(connection.getInputStream());
			BufferedReader reader = new BufferedReader(isr);

			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null)
			{
				sb.append(line).append("\n");
			}
			// Response from server after login process will be stored in response variable.
			String response = sb.toString();

			isr.close();
			reader.close();

			return response;
		}
		catch (IOException e)
		{
			throw new BadRequestException();
		}
	}
}

package edu.byu.colingm.familymap.dataobjects;

/**
 * author: colingm
 * date: 4/5/16.
 */
public class DisplayDTO
{
	private String name;
	private String gender;
	private String lifespan;
	private String birthDate;
	private String birthPlace;

	public DisplayDTO()
	{
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getLifespan()
	{
		return lifespan;
	}

	public void setLifespan(String lifespan)
	{
		this.lifespan = lifespan;
	}

	public String getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(String birthDate)
	{
		this.birthDate = birthDate;
	}

	public String getBirthPlace()
	{
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace)
	{
		this.birthPlace = birthPlace;
	}

	@Override
	public String toString()
	{
		return "DisplayDTO{" +
				"name='" + name + '\'' +
				", gender='" + gender + '\'' +
				", lifespan='" + lifespan + '\'' +
				", birthDate='" + birthDate + '\'' +
				", birthPlace='" + birthPlace + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DisplayDTO that = (DisplayDTO) o;

		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		if (gender != null ? !gender.equals(that.gender) : that.gender != null) return false;
		if (lifespan != null ? !lifespan.equals(that.lifespan) : that.lifespan != null) return false;
		if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
		return !(birthPlace != null ? !birthPlace.equals(that.birthPlace) : that.birthPlace != null);

	}

	@Override
	public int hashCode()
	{
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (gender != null ? gender.hashCode() : 0);
		result = 31 * result + (lifespan != null ? lifespan.hashCode() : 0);
		result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
		result = 31 * result + (birthPlace != null ? birthPlace.hashCode() : 0);
		return result;
	}
}

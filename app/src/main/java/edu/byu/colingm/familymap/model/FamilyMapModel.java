package edu.byu.colingm.familymap.model;

import android.content.SharedPreferences;
import android.util.Log;
import edu.byu.colingm.familymap.client.ClientFacade;
import edu.byu.colingm.familymap.dataobjects.*;
import edu.byu.colingm.familymap.dataobjects.types.Gender;
import edu.byu.colingm.familymap.dataobjects.types.Relationship;
import edu.byu.colingm.familymap.map.FactListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * author: colingm
 * date: 3/24/16.
 */
public class FamilyMapModel
{
	private static final String TAG = "FamilyMapModel";
	private static FamilyMapModel _instance;

	public static FamilyMapModel model()
	{
		if (_instance == null)
			_instance = new FamilyMapModel();

		return _instance;
	}

	public static ClientFacade client()
	{
		if (_instance == null)
			_instance = new FamilyMapModel();

		return _instance.getClientFacade();
	}

	private ClientFacade mClientFacade;
	private String mAccessToken;
	private User mCurrentUser;
	private Person mCurrentPerson;
	private Persons mPersons;
	private Facts mFacts;
	private boolean mLoaded;
	private int mPersonsLoading;
	private List<FactListener> mFactListeners;
	private Filters mFilters;
	private Map<String, String> mSelectedMarker;

	private FamilyMapModel()
	{
		mClientFacade = new ClientFacade();
		mPersons = new Persons();
		mFacts = new Facts();
		mPersonsLoading = 0;
		mFactListeners = new ArrayList<>();
		mFilters = new Filters();
		mSelectedMarker = new HashMap<>();
	}

	public void registerFactListener(FactListener listener)
	{
		mFactListeners.add(listener);
	}

	public String getAccessToken()
	{
		return mAccessToken;
	}

	public void setAccessToken(String token)
	{
		Log.e(TAG, "Token: " + token);
		mAccessToken = token;
	}

	public void clearAccessToken()
	{
		mAccessToken = null;
	}

	public void clearData()
	{
		mCurrentUser = null;
		mCurrentPerson = null;
		mPersons = new Persons();
		mFacts = new Facts();
		mPersonsLoading = 0;
	}

	public void setCurrentMarker(String factId, String personId)
	{
		mSelectedMarker.put("factId", factId);
		mSelectedMarker.put("personId", personId);
	}

	public Map<String, String> getCurrentMarker()
	{
		return mSelectedMarker;
	}

	public boolean isMarkerSelected()
	{
		return mSelectedMarker.containsKey("factId") && mSelectedMarker.containsKey("personId");
	}

	public void newPersonLoading()
	{
		mPersonsLoading++;
	}

	public void finishedPersonLoading()
	{
		if (mPersonsLoading > 0)
			mPersonsLoading--;

		if (mPersonsLoading == 0)
		{
			for (FactListener listener : mFactListeners)
				listener.notifyDoneLoading();
		}
	}

	public User getCurrentUser()
	{
		return mCurrentUser;
	}

	public void setCurrentUser(User currentUser)
	{
		Log.e(TAG, currentUser.toString());
		mCurrentUser = currentUser;
	}

	public ClientFacade getClientFacade()
	{
		return mClientFacade;
	}

	public void setClientFacade(ClientFacade clientFacade)
	{
		mClientFacade = clientFacade;
	}

	public Person getCurrentPerson()
	{
		return mCurrentPerson;
	}

	public FactDTO getFact(String factId, String personId)
	{
		return mFacts.getFact(factId + "::" + personId);
	}

	public Person getPerson(String personId)
	{
		return mPersons.getPerson(personId);
	}

	public void setCurrentPerson(Person currentPerson)
	{
		Log.e(TAG, currentPerson.toString());
		mCurrentPerson = currentPerson;
		mPersons.addPerson(currentPerson);
		addFactsForPerson(currentPerson);
	}

	public void setRelative(String personId, Relationship relationship, String relativeId)
	{
		mPersons.addRelationship(personId, relationship, relativeId);
	}

	public Person getRelative(String personId, Relationship relationship)
	{
		Person relative = mPersons.getRelatedPerson(personId, relationship);

		if (mFilters.canViewPerson(relative))
			return relative;
		else
			return null;
	}

	public void addFactsForPerson(Person person)
	{
		List<FactDTO> facts = person.getFacts();
		for (FactDTO fact : facts)
		{
			fact.setPersonId(person.getId());
			Log.e(TAG, fact.toString());
			mFacts.addFact(person.getId(), fact);
		}
	}

	public List<FactDTO> getLifeFactsForPerson(String personId)
	{
		List<FactDTO> facts = mFacts.getFactsForPerson(personId);
		if (facts == null || facts.isEmpty())
			return null;
		facts = filterVisibleFacts(facts);

		if (facts.isEmpty())
			return null;

		Collections.sort(facts, new FactComparator());

		return facts;
	}

	public FactDTO getEarliestFactForPerson(String personId)
	{
		List<FactDTO> facts = mFacts.getFactsForPerson(personId);
		facts = filterVisibleFacts(facts);

		if (facts.isEmpty())
			return null;

		Collections.sort(facts, new FactComparator());

		return facts.get(0);
	}

	public void addSpouse(String personId, Person spouse)
	{
		mPersons.addPerson(spouse);
		mPersons.addRelationship(personId, Relationship.Spouse, spouse.getId());
		addFactsForPerson(spouse);
		Log.e(TAG, spouse.toString());
	}

	public void addParents(String personId, List<Person> parents)
	{
		Log.e(TAG, parents.toString());
		for (Person parent : parents)
		{
			mPersons.addPerson(parent);
			if (parent.getGender() == Gender.Male)
				setRelative(personId, Relationship.Father, parent.getId());
			else if (parent.getGender() == Gender.Female)
				setRelative(personId, Relationship.Mother, parent.getId());

			addFactsForPerson(parent);
		}
	}

	public SearchData performSearch(String searchString)
	{
		List<FactDTO> facts = mFacts.getFactsForSearch(searchString);
		facts = filterVisibleFacts(facts);

		List<Person> persons = mPersons.searchPersons(searchString);
		persons = filterVisiblePeople(persons);

		return new SearchData(persons, facts);
	}

	public boolean isLoaded()
	{
		return mLoaded;
	}

	public void setLoaded(boolean loaded)
	{
		mLoaded = loaded;
	}

	public List<Person> filterVisiblePeople(List<Person> persons)
	{
		List<Person> visiblePersons = new ArrayList<>();
		if (persons == null)
			return visiblePersons;
		for (Person person : persons)
		{
			if (mFilters.canViewPerson(person))
				visiblePersons.add(person);
		}

		return visiblePersons;
	}

	public List<FactDTO> filterVisibleFacts(List<FactDTO> facts)
	{
		List<FactDTO> visibleFacts = new ArrayList<>();
		if (facts == null)
			return visibleFacts;
		for (FactDTO fact : facts)
		{
			if (mFilters.canViewEvent(fact))
				visibleFacts.add(fact);
		}

		return visibleFacts;
	}

	public List<FactDTO> getVisibleFacts()
	{
		return filterVisibleFacts(mFacts.getAllFacts());
	}

	public void setFilters(SharedPreferences preferences)
	{
		mFilters.setFilters(preferences);
	}

	static class FactComparator implements Comparator<FactDTO>
	{
		public int compare(FactDTO f1, FactDTO f2)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("d M y");

			try
			{
				Date oldDate = dateFormat.parse(f1.getDate());
				Date newDate = dateFormat.parse(f2.getDate());

				return oldDate.compareTo(newDate);
			}
			catch (ParseException ex)
			{
				return 0;
			}
		}
	}
}

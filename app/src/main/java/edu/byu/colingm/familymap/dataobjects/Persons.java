package edu.byu.colingm.familymap.dataobjects;

import edu.byu.colingm.familymap.dataobjects.types.Relationship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: colingm
 * date: 4/6/16.
 */
public class Persons
{
	private Map<String, PersonRelationships> relationships = new HashMap<>();
	private Map<String, Person> persons = new HashMap<>();

	public Persons()
	{
	}

	public void addRelationship(String personId, Relationship relationship, String relativeId)
	{
		if (!relationships.containsKey(personId))
			relationships.put(personId, new PersonRelationships());

		relationships.get(personId).addRelationship(relationship, relativeId);
	}

	public void addPerson(Person person)
	{
		if (!persons.containsKey(person.getId()))
		{
			persons.put(person.getId(), person);
		}
	}

	public Person getPerson(String personId)
	{
		return persons.get(personId);
	}

	public Person getRelatedPerson(String personId, Relationship relationship)
	{
		if (!relationships.containsKey(personId))
			return null;

		PersonRelationships personRelationships = relationships.get(personId);

		String relatedPersonId = personRelationships.getRelationship(relationship);

		if (relatedPersonId == null)
			return null;

		return persons.get(relatedPersonId);
	}

	public List<Person> searchPersons(String searchString)
	{
		List<Person> matching = new ArrayList<>();
		searchString = searchString.toLowerCase();
		for (Map.Entry<String, Person> entry : persons.entrySet())
		{
			Person person = entry.getValue();
			String name = person.getName().toLowerCase();

			if (name.contains(searchString))
				matching.add(person);
		}

		return matching;
	}

	@Override
	public String toString()
	{
		return "Persons{" +
				"relationships=" + relationships +
				", persons=" + persons +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Persons persons1 = (Persons) o;

		if (relationships != null ? !relationships.equals(persons1.relationships) : persons1.relationships != null)
			return false;
		return !(persons != null ? !persons.equals(persons1.persons) : persons1.persons != null);

	}

	@Override
	public int hashCode()
	{
		int result = relationships != null ? relationships.hashCode() : 0;
		result = 31 * result + (persons != null ? persons.hashCode() : 0);
		return result;
	}
}

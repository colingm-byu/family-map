package edu.byu.colingm.familymap.client;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import edu.byu.colingm.familymap.dataobjects.Person;
import edu.byu.colingm.familymap.dataobjects.User;
import edu.byu.colingm.familymap.dataobjects.types.Gender;
import edu.byu.colingm.familymap.model.FamilyMapModel;

import java.util.ArrayList;
import java.util.List;

/**
 * author: colingm
 * date: 4/5/16.
 */
public class UserGet extends AsyncTask<String, String, List<Person>>
{
	private ProgressDialog pDialog;
	private Activity mActivity;

	public UserGet(Activity activity)
	{
		super();
		mActivity = activity;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		pDialog = new ProgressDialog(mActivity);
		pDialog.setMessage("Loading Tree...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@Override
	protected List<Person> doInBackground(String... args)
	{
		FamilyMapModel model = FamilyMapModel.model();
		ClientFacade client = FamilyMapModel.client();

		User currentUser = client.getCurrentUser();
		model.setCurrentUser(currentUser);

		String personId = currentUser.getPersonId();

		Person person = client.getPerson(personId);
		model.setCurrentPerson(person);

		List<Person> persons = new ArrayList<>();
		List<Person> spouses = client.getRelatedPersons(personId, true);
		if (spouses != null && !spouses.isEmpty())
		{
			Person spouse = spouses.get(0);
			spouse.setIsParentAncestor(false);
			spouse.setFathersSide(false);
			model.addSpouse(personId, spouse);
			persons.add(spouse);
		}

		List<Person> parents = FamilyMapModel.client().getRelatedPersons(personId, false);
		if (parents != null && !parents.isEmpty())
		{
			model.addParents(personId, parents);
			persons.addAll(parents);
			for (Person parent : parents)
			{
				parent.setIsParentAncestor(true);
				if (parent.getGender() == Gender.Male)
					parent.setFathersSide(true);
				else
					parent.setFathersSide(false);
			}
		}

		model.setLoaded(true);
		return persons;
	}

	@Override
	protected void onPostExecute(List<Person> persons)
	{
		pDialog.dismiss();

		for (Person person : persons)
		{
			FamilyMapModel.model().newPersonLoading();
			new RelatedPersonsGet(mActivity, person.getId(), 1, person.isFathersSide(), person.isParentAncestor()).execute();
		}
	}
}

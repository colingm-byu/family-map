package edu.byu.colingm.familymap.dataobjects.types;

/**
 * author: colingm
 * date: 3/31/16.
 */
public enum Gender
{
	Male,
	Female
}

package edu.byu.colingm.familymap.client;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import edu.byu.colingm.familymap.dataobjects.Person;
import edu.byu.colingm.familymap.model.FamilyMapModel;

import java.util.List;

/**
 * author: colingm
 * date: 4/5/16.
 */
public class RelatedPersonsGet extends AsyncTask<String, String, List<Person>>
{
	private ProgressDialog pDialog;
	private Activity mActivity;
	private String mPersonId;
	private int mGeneration;
	private boolean mFathersSide;
	private boolean mParentAncestor;

	RelatedPersonsGet(Activity activity, String personId, int generation, boolean fathersSide, boolean parentAncestor)
	{
		super();
		mActivity = activity;
		mPersonId = personId;
		mGeneration = generation;
		mFathersSide = fathersSide;
		mParentAncestor = parentAncestor;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		pDialog = new ProgressDialog(mActivity);
		pDialog.setMessage("Loading Tree...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@Override
	protected List<Person> doInBackground(String... args)
	{
		FamilyMapModel model = FamilyMapModel.model();
		ClientFacade client = FamilyMapModel.client();

		List<Person> spouses = client.getRelatedPersons(mPersonId, true);
		if (spouses != null && !spouses.isEmpty())
		{
			Person spouse = spouses.get(0);
			spouse.setFathersSide(mFathersSide);
			spouse.setIsParentAncestor(mParentAncestor);
			model.addSpouse(mPersonId, spouse);
		}

		List<Person> parents = FamilyMapModel.client().getRelatedPersons(mPersonId, false);
		if (parents != null && !parents.isEmpty())
		{
			model.addParents(mPersonId, parents);
			for (Person person : parents)
			{
				person.setFathersSide(mFathersSide);
				person.setIsParentAncestor(mParentAncestor);
			}
		}

		return parents;
	}

	@Override
	protected void onPostExecute(List<Person> persons)
	{
		pDialog.dismiss();
		if (mGeneration < 5 && persons != null)
		{
			for (Person person : persons)
			{
				FamilyMapModel.model().newPersonLoading();
				new RelatedPersonsGet(mActivity, person.getId(), mGeneration + 1, mFathersSide, mParentAncestor).execute();
			}
		}
		FamilyMapModel.model().finishedPersonLoading();
	}
}

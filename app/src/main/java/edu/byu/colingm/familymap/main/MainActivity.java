package edu.byu.colingm.familymap.main;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import edu.byu.colingm.familymap.R;
import edu.byu.colingm.familymap.map.MapActivity;
import edu.byu.colingm.familymap.model.FamilyMapModel;
import edu.byu.colingm.familymap.oauth.GetAccessToken;
import edu.byu.colingm.familymap.utils.Helper;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
{
	private static final String TAG = "MainActivity";
	private static final String CLIENT_ID = "family_search_client_id";
	private static final String REDIRECT_URI = "family_search_redirect_uri";
	private static final String TOKEN_URI = "family_search_token_uri";
	private static final String OAUTH_URI = "family_search_oauth_uri";
	private static final String GRANT_TYPE = "authorization_code";
	private static final String OAUTH_STATE = "initial_login";
	private static final String RESPONSE_TYPE = "code";

	//Change the Scope as you need
	WebView web;
	Button auth;
	String authCode;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Helper.setConfigContext(this);
		setContentView(R.layout.activity_main);
		auth = (Button)findViewById(R.id.login);
		auth.setOnClickListener(loginClick);
	}


	private View.OnClickListener loginClick = new View.OnClickListener()
	{
		Dialog auth_dialog;

		@Override
		public void onClick(View arg0)
		{
			auth_dialog = new Dialog(MainActivity.this);
			auth_dialog.setContentView(R.layout.auth_dialog);
			web = (WebView) auth_dialog.findViewById(R.id.webv);
			web.getSettings().setJavaScriptEnabled(true);

			web.loadUrl(Helper.getConfigValue("fs_oauth_uri")
					+ "?redirect_uri=" + Helper.getConfigValue("fs_redirect_uri")
					+ "&response_type=" + RESPONSE_TYPE
					+ "&client_id=" + Helper.getConfigValue("fs_client_id")
					+ "&state=" + OAUTH_STATE);

			web.setWebViewClient(new WebViewClient()
			{

				boolean authComplete = false;
				Intent resultIntent = new Intent();

				@Override
				public void onPageStarted(WebView view, String url, Bitmap favicon)
				{
					super.onPageStarted(view, url, favicon);

				}

				@Override
				public void onPageFinished(WebView view, String url)
				{
					super.onPageFinished(view, url);

					if (url.contains("?code=") && !authComplete)
					{
						Uri uri = Uri.parse(url);
						if (!OAUTH_STATE.equals(uri.getQueryParameter("state")))
						{
							processError();
						}
						else
						{
							authCode = uri.getQueryParameter("code");
							Log.i(TAG, "CODE : " + authCode);
							authComplete = true;
							resultIntent.putExtra("code", authCode);
							MainActivity.this.setResult(Activity.RESULT_OK, resultIntent);
							setResult(Activity.RESULT_CANCELED, resultIntent);

							auth_dialog.dismiss();
							new TokenGet(authCode).execute();
//							Toast.makeText(getApplicationContext(), "Authorization mCode is: " + authCode, Toast.LENGTH_SHORT).show();
						}
					}
					else if (url.contains("error="))
					{
						processError();
					}
				}

				private void processError()
				{
					Log.i(TAG, "Unknown error occurred");
					resultIntent.putExtra("code", authCode);
					authComplete = true;
					setResult(Activity.RESULT_CANCELED, resultIntent);
					Toast.makeText(getApplicationContext(), "Error Occurred", Toast.LENGTH_SHORT).show();

					auth_dialog.dismiss();
				}
			});

			auth_dialog.show();
			auth_dialog.setTitle("Login to FamilySearch");
			auth_dialog.setCancelable(true);
		}
	};

	private class TokenGet extends AsyncTask<String, String, JSONObject>
	{
		private ProgressDialog pDialog;
		String mCode;

		TokenGet(String authCode)
		{
			super();
			mCode = authCode;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Contacting Family Search ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected JSONObject doInBackground(String... args)
		{
			GetAccessToken jParser = new GetAccessToken();
			return jParser.getToken(Helper.getConfigValue("fs_token_uri"),
					mCode,
					Helper.getConfigValue("fs_client_id"),
					Helper.getConfigValue("fs_redirect_uri"),
					GRANT_TYPE);
		}

		@Override
		protected void onPostExecute(JSONObject json)
		{
			pDialog.dismiss();
			if (json != null)
			{

				try
				{
					if (json.has("access_token"))
					{
						String tok = json.getString("access_token");

						FamilyMapModel.model().setAccessToken(tok);
						Intent intent = new Intent(MainActivity.this, MapActivity.class);
						startActivity(intent);
					}
					else
					{
						String error = json.getString("error");
						String description = json.getString("error_description");

						Toast.makeText(getApplicationContext(), "Unable to login: " + description, Toast.LENGTH_SHORT).show();
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}

			}
			else
			{
				Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
				pDialog.dismiss();
			}
		}
	}
}

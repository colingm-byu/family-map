package edu.byu.colingm.familymap.map;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.byu.colingm.familymap.R;
import edu.byu.colingm.familymap.dataobjects.FactDTO;
import edu.byu.colingm.familymap.dataobjects.Person;

/**
 * author: colingm
 * date: 4/12/16.
 */
public class EventFragment extends Fragment implements View.OnClickListener
{
	private Person mPerson;
	private FactDTO mFactDTO;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_event, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		View genderView = getActivity().findViewById(R.id.genderImage);
		if (genderView != null)
			genderView.setOnClickListener(this);
		View eventDescription = getActivity().findViewById(R.id.event_description);
		if (eventDescription != null)
			eventDescription.setOnClickListener(this);
	}

	public void updateEventInformation(Person person, FactDTO fact)
	{
		mPerson = person;
		mFactDTO = fact;
	}

	@Override
	public void onClick(View v)
	{
		if (mPerson != null && mFactDTO != null)
			Log.e("EventFragment", "Person: " + mPerson + ", Fact: " + mFactDTO);
		else
			Log.e("EventFragment", "Too Early");
	}
}

package edu.byu.colingm.familymap.dataobjects.types;

/**
 * author: colingm
 * date: 4/6/16.
 */
public enum Relationship
{
	Spouse,
	Father,
	Mother
}

package edu.byu.colingm.familymap.dataobjects;

import edu.byu.colingm.familymap.dataobjects.types.Gender;

/**
 * author: colingm
 * date: 3/31/16.
 */
public class User
{
	private String id;
	private String givenName;
	private String familyName;
	private String email;
	private String country;
	private String gender;
	private String birthDate;
	private String preferredLanguage;
	private String displayName;
	private String personId;

	public User()
	{
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getFamilyName()
	{
		return familyName;
	}

	public void setFamilyName(String familyName)
	{
		this.familyName = familyName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public Gender getGender()
	{
		return Gender.valueOf(this.gender);
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(String birthDate)
	{
		this.birthDate = birthDate;
	}

	public String getPreferredLanguage()
	{
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage)
	{
		this.preferredLanguage = preferredLanguage;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getPersonId()
	{
		return personId;
	}

	public void setPersonId(String personId)
	{
		this.personId = personId;
	}

	@Override
	public String toString()
	{
		return "User{" +
				"id='" + id + '\'' +
				", givenName='" + givenName + '\'' +
				", familyName='" + familyName + '\'' +
				", email='" + email + '\'' +
				", country='" + country + '\'' +
				", gender='" + gender + '\'' +
				", birthDate='" + birthDate + '\'' +
				", preferredLanguage='" + preferredLanguage + '\'' +
				", displayName='" + displayName + '\'' +
				", personId='" + personId + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (id != null ? !id.equals(user.id) : user.id != null) return false;
		if (givenName != null ? !givenName.equals(user.givenName) : user.givenName != null) return false;
		if (familyName != null ? !familyName.equals(user.familyName) : user.familyName != null) return false;
		if (email != null ? !email.equals(user.email) : user.email != null) return false;
		if (country != null ? !country.equals(user.country) : user.country != null) return false;
		if (gender != null ? !gender.equals(user.gender) : user.gender != null) return false;
		if (birthDate != null ? !birthDate.equals(user.birthDate) : user.birthDate != null) return false;
		if (preferredLanguage != null ? !preferredLanguage.equals(user.preferredLanguage) : user.preferredLanguage != null)
			return false;
		if (displayName != null ? !displayName.equals(user.displayName) : user.displayName != null) return false;
		return !(personId != null ? !personId.equals(user.personId) : user.personId != null);

	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (givenName != null ? givenName.hashCode() : 0);
		result = 31 * result + (familyName != null ? familyName.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (country != null ? country.hashCode() : 0);
		result = 31 * result + (gender != null ? gender.hashCode() : 0);
		result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
		result = 31 * result + (preferredLanguage != null ? preferredLanguage.hashCode() : 0);
		result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
		result = 31 * result + (personId != null ? personId.hashCode() : 0);
		return result;
	}
}
package edu.byu.colingm.familymap.settings;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import com.thebluealliance.spectrum.SpectrumPreference;
import edu.byu.colingm.familymap.R;
import edu.byu.colingm.familymap.client.UserGet;
import edu.byu.colingm.familymap.main.MainActivity;
import edu.byu.colingm.familymap.model.FamilyMapModel;

/**
 * author: colingm
 * date: 3/25/16.
 */
public class SettingsActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_settings);
		getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
	}

	public void logout()
	{
		FamilyMapModel.model().clearAccessToken();
		Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
		startActivity(intent);
	}

	public static class SettingsFragment extends PreferenceFragment
	{
		public SettingsFragment()
		{
		}

		@Override
		public void onCreate(final Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.preferences);
			SpectrumPreference colorPreference = (SpectrumPreference) findPreference(getString(R.string.life_story_color_key));
			colorPreference.setColors(R.array.spectrumColorsArray);
			colorPreference = (SpectrumPreference) findPreference(getString(R.string.spouse_lines_color_key));
			colorPreference.setColors(R.array.spectrumColorsArray);
			colorPreference = (SpectrumPreference) findPreference(getString(R.string.family_tree_color_key));
			colorPreference.setColors(R.array.spectrumColorsArray);

			Preference preference = findPreference(getString(R.string.logout_key));
			preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					((SettingsActivity) getActivity()).logout();
					return true;
				}
			});

			preference = findPreference(getString(R.string.sync_data_key));
			preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					FamilyMapModel.model().clearData();
					new UserGet(getActivity()).execute();
					return true;
				}
			});
		}
	}
}

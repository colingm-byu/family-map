package edu.byu.colingm.familymap.utils;

import android.content.Context;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.vision.barcode.Barcode;
import edu.byu.colingm.familymap.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * author: colingm
 * date: 3/25/16.
 */
public final class Helper
{
	private static final String TAG = "Helper";
	private static Properties _properties;
	private static Context sContext;
	private static Map<String, LatLng> mLocationCache = new HashMap<>();

	public static void setConfigContext(Context context)
	{
		sContext = context;
	}

	public static String getConfigValue(String name)
	{
		if (_properties == null)
		{
			Resources resources = sContext.getResources();

			try
			{
				InputStream rawResource = resources.openRawResource(R.raw.config);
				_properties = new Properties();
				_properties.load(rawResource);
			}
			catch (Resources.NotFoundException e)
			{
				Log.e(TAG, "Unable to find the config file: " + e.getMessage());
			}
			catch (IOException e)
			{
				Log.e(TAG, "Failed to open config file.");
			}
		}

		return _properties.getProperty(name);
	}

	public static LatLng getLocationFromAddress(Context context, String strAddress)
	{
		// TODO: IMPROVE CACHE TO ONLY STORE MOST USED AND CERTAIN AMOUNT OF ADDRESSES
		if (mLocationCache.containsKey(strAddress))
			return mLocationCache.get(strAddress);

		Geocoder coder = new Geocoder(context);
		List<Address> address;

		try
		{
			address = coder.getFromLocationName(strAddress, 5);
			if (address == null)
			{
				return null;
			}
			Address location = address.get(0);

			LatLng coordinates = new LatLng(location.getLatitude(), location.getLongitude());
			mLocationCache.put(strAddress, coordinates);

			return coordinates;
		}
		catch (IOException ex)
		{
			// LEAVE EMPTY
			return null;
		}
	}
}

package edu.byu.colingm.familymap.oauth;

import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * author: colingm
 * date: 3/24/16.
 */

public class GetAccessToken
{
	private static final String TAG = "GetAccessToken";
	private static JSONObject jObj = null;

	public GetAccessToken()
	{
	}

	private List<NameValuePair> params = new ArrayList<>();

	public static String executePost(String targetURL, String urlParameters)
	{
		URL url;
		HttpURLConnection connection = null;
		try
		{
			//Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" +
					Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			//Send request
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			//Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			String line;
			StringBuilder response = new StringBuilder();
			while ((line = rd.readLine()) != null)
			{
				response.append(line).append("\n");
			}
			rd.close();
			return response.toString();

		}
		catch (Exception e)
		{

			e.printStackTrace();
			return null;

		}
		finally
		{

			if (connection != null)
			{
				connection.disconnect();
			}
		}
	}

	public JSONObject getToken(String address, String token, String client_id, String redirect_uri, String grant_type)
	{
		try
		{

			params.add(new BasicNameValuePair("code", token));
			params.add(new BasicNameValuePair("client_id", client_id));
			params.add(new BasicNameValuePair("redirect_uri", redirect_uri));
			params.add(new BasicNameValuePair("grant_type", grant_type));

			String json = executePost(address, getQuery(params));
			Log.d("JSONStr", json);

			jObj = new JSONObject(json);

		}
		catch (JSONException e)
		{
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
		catch (UnsupportedEncodingException e)
		{
			Log.e(TAG, "UnsupportedEncodingException", e);
		}

		// Return JSON String
		return jObj;
	}

	private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
	{
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (NameValuePair pair : params)
		{
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
		}

		return result.toString();
	}
}

package edu.byu.colingm.familymap.dataobjects;

import android.content.SharedPreferences;
import edu.byu.colingm.familymap.dataobjects.types.Event;
import edu.byu.colingm.familymap.dataobjects.types.Gender;
import edu.byu.colingm.familymap.model.FamilyMapModel;

/**
 * author: colingm
 * date: 4/18/16.
 */
public class Filters
{
	private boolean viewBirths;
	private boolean viewChristenings;
	private boolean viewDeaths;
	private boolean viewFathersSide;
	private boolean viewMothersSide;
	private boolean viewMales;
	private boolean viewFemales;

	public void setFilters(SharedPreferences preferences)
	{
		viewBirths = preferences.getBoolean("birthEvents", true);
		viewChristenings = preferences.getBoolean("christeningEvents", true);
		viewDeaths = preferences.getBoolean("deathEvents", true);
		viewFathersSide = preferences.getBoolean("fathersEvents", true);
		viewMothersSide = preferences.getBoolean("mothersEvents", true);
		viewMales = preferences.getBoolean("maleEvents", true);
		viewFemales = preferences.getBoolean("femaleEvents", true);
	}

	public boolean canViewPerson(Person person)
	{
		boolean canViewEvent = true;
		if (person == null)
			return false;

		Gender gender = person.getGender();
		switch (gender)
		{
			case Male:
				if (!viewMales)
					canViewEvent = false;
				break;
			case Female:
				if (!viewFemales)
					canViewEvent = false;
				break;
			default:
				canViewEvent = false;
				break;
		}

		if (person.isParentAncestor())
		{
			if (person.isFathersSide() && !viewFathersSide)
				canViewEvent = false;
			else if (!person.isFathersSide() && !viewMothersSide)
				canViewEvent = false;
		}

		return canViewEvent;
	}

	public boolean canViewEvent(FactDTO fact)
	{
		Event type = fact.getType();
		boolean canViewEvent = true;
		switch (type)
		{
			case Birth:
				if (!viewBirths)
					canViewEvent = false;
				break;
			case Christening:
				if (!viewChristenings)
					canViewEvent = false;
				break;
			case Death:
				if (!viewDeaths)
					canViewEvent = false;
				break;
			default:
				canViewEvent = false;
				break;
		}

		if (!canViewEvent)
			return false;

		Person person = FamilyMapModel.model().getPerson(fact.getPersonId());
		canViewEvent = canViewPerson(person);

		return canViewEvent;
	}
}

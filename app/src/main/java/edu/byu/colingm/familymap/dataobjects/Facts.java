package edu.byu.colingm.familymap.dataobjects;

import java.util.*;

/**
 * author: colingm
 * date: 4/16/16.
 */
public class Facts
{
	Map<String, Set<String>> mPersonEvents = new HashMap<>();
	Map<String, String> mFactsToPerson = new HashMap<>();
	Map<String, FactDTO> mFacts = new HashMap<>();

	public void addFact(String personId, FactDTO fact)
	{
		String factId = fact.getId() + "::" + personId;
		if (!mPersonEvents.containsKey(personId))
			mPersonEvents.put(personId, new HashSet<String>());

		mPersonEvents.get(personId).add(factId);

		if (!mFacts.containsKey(factId))
		{
			mFacts.put(factId, fact);
		}
		if (!mFactsToPerson.containsKey(factId))
			mFactsToPerson.put(factId, personId);
	}

	public FactDTO getFact(String factId)
	{
		return mFacts.get(factId);
	}

	public String getPersonIdForFact(String factId)
	{
		return mFactsToPerson.get(factId);
	}

	public List<FactDTO> getAllFacts()
	{
		List<FactDTO> allFacts = new ArrayList<>();
		for (Map.Entry<String, FactDTO> entry : mFacts.entrySet())
		{
			allFacts.add(entry.getValue());
		}

		return allFacts;
	}

	public List<FactDTO> getFactsForPerson(String personId)
	{
		Set<String> factIds = mPersonEvents.get(personId);

		List<FactDTO> facts = new ArrayList<>();
		if (factIds != null && !factIds.isEmpty())
		{
			for (String factId : factIds)
			{
				if (mFacts.containsKey(factId))
					facts.add(mFacts.get(factId));
			}
		}

		return facts;
	}

	public List<FactDTO> getFactsForSearch(String searchString)
	{
		List<FactDTO> facts = new ArrayList<>();
		searchString = searchString.toLowerCase();
		for (Map.Entry<String, FactDTO> entry : mFacts.entrySet())
		{
			FactDTO fact = entry.getValue();
			String date = fact.getDate().toLowerCase();
			String location = fact.getPlace().toLowerCase();

			if (date.contains(searchString) || location.contains(searchString))
				facts.add(fact);
		}

		return facts;
	}
}

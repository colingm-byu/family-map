package edu.byu.colingm.familymap.dataobjects;

import edu.byu.colingm.familymap.dataobjects.types.Gender;

import java.util.List;

/**
 * author: colingm
 * date: 3/31/16.
 */
public class Person
{
	private String id;
	private Boolean living;
	private DisplayDTO display;
	private List<FactDTO> facts;
	private boolean fathersSide;
	private boolean isParentAncestor;

	public Person() {}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Boolean getLiving()
	{
		return living;
	}

	public void setLiving(Boolean living)
	{
		this.living = living;
	}

	public String getBirthDate()
	{
		return display.getBirthDate();
	}

	public String getBirthPlace()
	{
		return display.getBirthPlace();
	}

	public String getName()
	{
		return display.getName();
	}

	public void setName(String name)
	{
		display.setName(name);
	}

	public Gender getGender()
	{
		return Gender.valueOf(display.getGender());
	}

	public void setGender(String gender)
	{
		display.setGender(gender);
	}

	public DisplayDTO getDisplay()
	{
		return display;
	}

	public void setDisplay(DisplayDTO display)
	{
		this.display = display;
	}

	public List<FactDTO> getFacts()
	{
		return facts;
	}

	public void setFacts(List<FactDTO> facts)
	{
		this.facts = facts;
	}

	public boolean isFathersSide()
	{
		return fathersSide;
	}

	public void setFathersSide(boolean fathersSide)
	{
		this.fathersSide = fathersSide;
	}

	public boolean isParentAncestor()
	{
		return isParentAncestor;
	}

	public void setIsParentAncestor(boolean isParentAncestor)
	{
		this.isParentAncestor = isParentAncestor;
	}

	@Override
	public String toString()
	{
		return "Person{" +
				"id='" + id + '\'' +
				", living=" + living +
				", display=" + display +
				", facts=" + facts +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Person person = (Person) o;

		if (id != null ? !id.equals(person.id) : person.id != null) return false;
		if (living != null ? !living.equals(person.living) : person.living != null) return false;
		if (display != null ? !display.equals(person.display) : person.display != null) return false;
		return !(facts != null ? !facts.equals(person.facts) : person.facts != null);

	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (living != null ? living.hashCode() : 0);
		result = 31 * result + (display != null ? display.hashCode() : 0);
		result = 31 * result + (facts != null ? facts.hashCode() : 0);
		return result;
	}
}
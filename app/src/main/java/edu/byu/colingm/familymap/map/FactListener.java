package edu.byu.colingm.familymap.map;

import edu.byu.colingm.familymap.dataobjects.FactDTO;

/**
 * author: colingm
 * date: 4/18/16.
 */
public interface FactListener
{
	void notifyDoneLoading();
}

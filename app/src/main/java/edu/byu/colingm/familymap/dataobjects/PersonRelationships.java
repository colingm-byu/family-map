package edu.byu.colingm.familymap.dataobjects;

import android.util.Log;
import edu.byu.colingm.familymap.dataobjects.types.Relationship;

/**
 * author: colingm
 * date: 4/6/16.
 */
public class PersonRelationships
{
	private String spouse;
	private String father;
	private String mother;

	public PersonRelationships(String spouse, String father, String mother)
	{
		this.spouse = spouse;
		this.father = father;
		this.mother = mother;
	}

	public PersonRelationships()
	{

	}

	public void addRelationship(Relationship relationship, String relativeId)
	{
		switch (relationship)
		{
			case Spouse:
				spouse = relativeId;
				break;
			case Father:
				father = relativeId;
				break;
			case Mother:
				mother = relativeId;
				break;
			default:
				Log.e("PersonRelationships", "Unknown relationship to add");
				break;
		}
	}

	public String getRelationship(Relationship relationship)
	{
		String personId = null;
		switch (relationship)
		{
			case Spouse:
				personId = spouse;
				break;
			case Father:
				personId = father;
				break;
			case Mother:
				personId = mother;
				break;
			default:
				Log.e("PersonRelationships", "Unknown relationship to add");
				break;
		}

		return personId;
	}

	public String getSpouse()
	{

		return spouse;
	}

	public void setSpouse(String spouse)
	{
		this.spouse = spouse;
	}

	public String getFather()
	{
		return father;
	}

	public void setFather(String father)
	{
		this.father = father;
	}

	public String getMother()
	{
		return mother;
	}

	public void setMother(String mother)
	{
		this.mother = mother;
	}

	@Override
	public String toString()
	{
		return "PersonRelationships{" +
				"spouse='" + spouse + '\'' +
				", father='" + father + '\'' +
				", mother='" + mother + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PersonRelationships that = (PersonRelationships) o;

		if (spouse != null ? !spouse.equals(that.spouse) : that.spouse != null) return false;
		if (father != null ? !father.equals(that.father) : that.father != null) return false;
		return !(mother != null ? !mother.equals(that.mother) : that.mother != null);

	}

	@Override
	public int hashCode()
	{
		int result = spouse != null ? spouse.hashCode() : 0;
		result = 31 * result + (father != null ? father.hashCode() : 0);
		result = 31 * result + (mother != null ? mother.hashCode() : 0);
		return result;
	}
}

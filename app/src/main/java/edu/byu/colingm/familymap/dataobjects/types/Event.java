package edu.byu.colingm.familymap.dataobjects.types;

/**
 * author: colingm
 * date: 4/18/16.
 */
public enum Event
{
	Birth,
	Christening,
	Death,
	Unknown
}

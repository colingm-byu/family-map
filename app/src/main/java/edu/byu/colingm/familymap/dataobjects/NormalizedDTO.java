package edu.byu.colingm.familymap.dataobjects;

import java.util.List;
import java.util.Map;

/**
 * author: colingm
 * date: 4/5/16.
 */
public class NormalizedDTO
{
	private String original;
	private List<Map<String, String>> normalized;

	public NormalizedDTO()
	{
	}

	public void setNormalized(List<Map<String, String>> normalized)
	{
		this.normalized = normalized;
	}

	public String getValue()
	{
		return original;
//		if (normalized == null || normalized.isEmpty())
//			return original;
//
//		return normalized.get(0).get("value");
	}

	@Override
	public String toString()
	{
		return "NormalizedDTO{" +
				"original='" + original + '\'' +
				", normalized=" + normalized +
				'}';
	}
}
